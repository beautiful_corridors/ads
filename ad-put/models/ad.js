/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Ad', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    advertiser_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    advertiser: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    type: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    },
    size_type_name: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    width: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    height: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    click_url: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    target_window: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    file_source_type: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '1'
    },
    template_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    content: {
      type: DataTypes.STRING(4000),
      allowNull: false
    },
    content_ext: {
      type: DataTypes.STRING(4000),
      allowNull: true
    },
    style: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    enabled: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '1'
    },
    remark: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    create_by: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: true
    },
    update_by: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'ads_ad'
  });
};
