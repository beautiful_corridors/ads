package io.finer.ads.jeecg.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import io.finer.ads.jeecg.entity.Channel;
import io.finer.ads.jeecg.service.IChannelService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 广告频道
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Api(tags="广告频道")
@RestController
@RequestMapping("/channel")
@Slf4j
public class ChannelController extends JeecgController<Channel, IChannelService> {
	@Autowired
	private IChannelService channelService;

	/**
	 * 分页列表查询
	 *
	 * @param channel
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "广告频道-分页列表查询")
	@ApiOperation(value="广告频道-分页列表查询", notes="广告频道-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(Channel channel,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Channel> queryWrapper = QueryGenerator.initQueryWrapper(channel, req.getParameterMap());
		Page<Channel> page = new Page<Channel>(pageNo, pageSize);
		IPage<Channel> pageList = channelService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param channel
	 * @return
	 */
	@AutoLog(value = "广告频道-添加")
	@ApiOperation(value="广告频道-添加", notes="广告频道-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody Channel channel) {
		channelService.save(channel);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param channel
	 * @return
	 */
	@AutoLog(value = "广告频道-编辑")
	@ApiOperation(value="广告频道-编辑", notes="广告频道-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody Channel channel) {
		channelService.updateById(channel);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "广告频道-通过id删除")
	@ApiOperation(value="广告频道-通过id删除", notes="广告频道-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		channelService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "广告频道-批量删除")
	@ApiOperation(value="广告频道-批量删除", notes="广告频道-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.channelService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "广告频道-通过id查询")
	@ApiOperation(value="广告频道-通过id查询", notes="广告频道-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		Channel channel = channelService.getById(id);
		if(channel==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(channel);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param channel
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Channel channel) {
        return super.exportXls(request, channel, Channel.class, "广告频道");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Channel.class);
    }

}
