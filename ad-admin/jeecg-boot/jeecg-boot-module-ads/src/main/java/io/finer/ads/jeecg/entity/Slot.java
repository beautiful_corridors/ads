package io.finer.ads.jeecg.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 广告位
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Data
@TableName("ads_slot")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ads_slot对象", description="广告位")
public class Slot implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "ID")
    private java.lang.String id;
	/**名称*/
	@Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    private java.lang.String name;
	/**频道*/
	@Excel(name = "频道", width = 15, dictTable = "ads_channel", dicText = "name", dicCode = "id")
	@Dict(dictTable = "ads_channel", dicText = "name", dicCode = "id")
    @ApiModelProperty(value = "频道")
    private java.lang.String channelId;
	/**终端类型*/
	@Excel(name = "终端类型", width = 15, dicCode = "ads_media_type")
	@Dict(dicCode = "ads_media_type")
    @ApiModelProperty(value = "终端类型")
    private java.lang.String mediaType;
	/**类型*/
	@Excel(name = "类型", width = 15, dicCode = "ads_slot_type")
	@Dict(dicCode = "ads_slot_type")
    @ApiModelProperty(value = "类型")
    private java.lang.Integer type;
	/**尺寸类型*/
	@Excel(name = "尺寸类型", width = 15)
    @ApiModelProperty(value = "尺寸类型")
    private java.lang.String sizeTypeName;
	/**宽 px*/
	@Excel(name = "宽 px", width = 15)
    @ApiModelProperty(value = "宽 px")
    private java.lang.Integer width;
	/**高 px*/
	@Excel(name = "高 px", width = 15)
    @ApiModelProperty(value = "高 px")
    private java.lang.Integer height;
	/**文件类型*/
	@Excel(name = "文件类型", width = 15)
    @ApiModelProperty(value = "文件类型")
    private java.lang.String fileTypes;
	/**文件最大KB*/
	@Excel(name = "文件最大KB", width = 15)
    @ApiModelProperty(value = "文件最大KB")
    private java.lang.Integer fileMax;
	/**位置说明*/
	@Excel(name = "位置说明", width = 15)
    @ApiModelProperty(value = "位置说明")
    private java.lang.String position;
	/**水平对齐*/
	@Excel(name = "水平对齐", width = 15, dicCode = "ads_align_h")
	@Dict(dicCode = "ads_align_h")
    @ApiModelProperty(value = "水平对齐")
    private java.lang.String alignH;
	/**水平边距 px*/
	@Excel(name = "水平边距 px", width = 15)
    @ApiModelProperty(value = "水平边距 px")
    private java.lang.Integer marginH;
	/**垂直对齐*/
	@Excel(name = "垂直对齐", width = 15, dicCode = "ads_align_v")
	@Dict(dicCode = "ads_align_v")
    @ApiModelProperty(value = "垂直对齐")
    private java.lang.String alignV;
	/**垂直边距 px*/
	@Excel(name = "垂直边距 px", width = 15)
    @ApiModelProperty(value = "垂直边距 px")
    private java.lang.Integer marginV;
	/**停留时间（秒）*/
	@Excel(name = "停留时间（秒）", width = 15)
    @ApiModelProperty(value = "停留时间（秒）")
    private java.lang.Integer stayTime;
	/**是否跟随滚动条*/
	@Excel(name = "是否跟随滚动条", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否跟随滚动条")
    private java.lang.Integer scrolled;
	/**是否有关闭按钮*/
	@Excel(name = "是否有关闭按钮", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否有关闭按钮")
    private java.lang.Integer closable;
	/**调度方式*/
	@Excel(name = "调度方式", width = 15, dicCode = "ads_schedule_mode")
	@Dict(dicCode = "ads_schedule_mode")
    @ApiModelProperty(value = "调度方式")
    private java.lang.Integer scheduleMode;
	/**一次展示广告数*/
	@Excel(name = "一次展示广告数", width = 15)
    @ApiModelProperty(value = "一次展示广告数")
    private java.lang.Integer adCount;
	/**轮换间隔（秒）*/
	@Excel(name = "轮换间隔（秒）", width = 15)
    @ApiModelProperty(value = "轮换间隔（秒）")
    private java.lang.Integer rotateInterval;
	/**每天显示次数*/
	@Excel(name = "每天显示次数", width = 15)
    @ApiModelProperty(value = "每天显示次数")
    private java.math.BigDecimal dayTimes;
	/**启用*/
	@Excel(name = "启用", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "启用")
    private java.lang.Integer enabled;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remark;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
    private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**修改者*/
    @ApiModelProperty(value = "修改者")
    private java.lang.String updateBy;
	/**修改时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date updateTime;
}
